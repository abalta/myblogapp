package apps.abdullahbalta.com.abdullahbaltablogapp;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.abdullahbalta.apps.R;

public class SplashActivity extends AppCompatActivity {

    private static final String BLOG_URL = "http://abdullahbalta.com";
    private Uri uri;
    private CustomTabsIntent.Builder customTabsIntent;
    private ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        connectionDetector = new ConnectionDetector(SplashActivity.this);
        uri = Uri.parse(BLOG_URL);
        customTabsIntent = new CustomTabsIntent.Builder();
        customTabsIntent.setToolbarColor(ContextCompat.getColor(SplashActivity.this, R.color.colorPrimary));

        if(connectionDetector.isConnectingToInternet()) {
            customTabsIntent.build().launchUrl(SplashActivity.this, uri);
            finish();
        }else{
            showAlertDialog(SplashActivity.this, "Uyarı", "Lütfen internet bağlantınızı kontrol ediniz.");
        }
    }

    public void showAlertDialog(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        alertDialog.setCancelable(false);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Çıkış Yap", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
